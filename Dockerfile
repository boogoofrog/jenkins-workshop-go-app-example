# syntax = docker/dockerfile:experimental
FROM golang:1.12-alpine as builder

RUN apk update && \
    apk add --no-cache git curl ca-certificates && \
    update-ca-certificates

RUN adduser -D -g '' appuser

WORKDIR $GOPATH/src/httpd

ENV CGO_ENABLED=0

COPY . .
RUN --mount=type=cache,target=/root/.cache/go-build go build -o /go/bin/httpd -ldflags '-extldflags "-static"' httpd.go
# Second step to build minimal image
FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd

USER appuser

COPY --from=builder /go/bin/httpd /go/bin/httpd

EXPOSE 8080
ENTRYPOINT ["/go/bin/httpd"]
